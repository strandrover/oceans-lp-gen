const gulp = require("gulp");
const shell = require("gulp-shell");
const del = require("del");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass");
const notify = require("gulp-notify");
const size = require("gulp-size");
const autoprefixer = require("gulp-autoprefixer");
const cssnano = require("gulp-cssnano");
const bust = require("gulp-buster");
const stylelint = require("gulp-stylelint");
const jsonFormat = require("gulp-json-format");
/**
 * Building Jekyll Site
 */

// Runs the build command for Jekyll to compile the site locally
// This will build the site with the production settings
gulp.task("jekyll:dev", shell.task("bundle exec jekyll build"));

gulp.task("jekyll-rebuild", ["jekyll:dev"], function() {
  browserSync.reload();
});

// Almost identical to the above task, but instead we load in the build configuration
// that overwrites some of the settings in the regular configuration so that you
// don"t end up publishing your drafts or future posts
gulp.task(
  "jekyll:staging",
  ["cachebust"],
  shell.task("bundle exec jekyll build --config _config.yml,_config.staging.yml")
);

// These tasks will look for files that change while serving and will auto-regenerate or
// reload the website accordingly. Update or add other files you need to be watched.
gulp.task("watch", function() {
  gulp.watch(["src/**/*.md", "src/**/*.html", "src/**/*.xml", "src/**/*.txt", "src/**/*.js"], ["jekyll-rebuild"]);

  gulp.watch(["src/assets/scss/**/*.scss"], ["styles"]);
});

// BrowserSync will serve our site on a local server for us and other devices to use
// It will also autoreload across all devices as well as keep the viewport synchronized
// between them.
gulp.task("serve:dev", ["styles", "jekyll:dev"], function() {
  browserSync.init({
    notify: true,
    browser: "google chrome",
    server: {
      baseDir: "dev"
    }
  });
});

/**
 * Compiling SASS files
 */

gulp.task("styles", ["lint-scss"], function() {
  // Looks at the style.scss file for what to include and creates a style.css file
  return gulp
    .src("src/assets/scss/screen.scss")
    .on("error", errorHandler)
    .on("error", notify.onError())
    .pipe(
      sass({
        includePaths: require("node-normalize-scss").includePaths
      })
    )
    .pipe(gulp.dest("src/assets/css/"))
    .pipe(gulp.dest("dev/assets/css/"))
    .pipe(size({ title: "styles" }))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("styles:prod", function() {
  return gulp
    .src("src/assets/scss/*.scss")
    .pipe(
      sass({
        includePaths: require("node-normalize-scss").includePaths,
        outputStyle: "compressed"
      })
    )
    .pipe(
      autoprefixer({
        browsers: ["last 4 versions"],
        cascade: false
      })
    )
    .pipe(cssnano())
    .pipe(gulp.dest("src/assets/css/"))
    .pipe(size({ title: "styles" }));
});

gulp.task("lint-scss", function lintCssTask() {
  return gulp.src("src/assets/**/*.scss").pipe(
    stylelint({
      reporters: [{ formatter: "string", console: true }]
    })
  );
});

/**
 * Cache busting
 */
gulp.task("cachebust", ["styles:prod"], function() {
  return gulp
    .src(["src/assets/css/screen.css", "src/assets/**/*.js"])
    .pipe(bust({ relativePath: "src/assets/" }))
    .pipe(gulp.dest("src/_data/"));
});

/**
 * Download pages data from cms
 */
gulp.task(
  "download-cms-data",
  shell.task([
    "curl -u r3S9Sh2ZfnfCpiwLKu9VIK17FIU9IuqW: https://cms.strandrover.com/api/1.1/tables/pages/rows -o src/_data/cms.json"
  ])
);

gulp.task("cms", ["download-cms-data"], function() {
  return gulp
    .src("src/_data/cms.json")
    .pipe(jsonFormat(2))
    .pipe(gulp.dest("src/_data/"));
});

/**
 * Error handling on sass compilation for example
 */
function errorHandler(error) {
  console.log(error.toString());
  this.emit("end");
}

/**
 * Gulp workflow
 */

// Default task, run when just writing "gulp" in the terminal
gulp.task("default", ["serve:dev", "watch"]);

gulp.task("build:staging", ["jekyll:staging"]);
